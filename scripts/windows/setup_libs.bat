@ECHO OFF
set LIB_DIR=libs
set QT_DIR=qt_build
set GOOGLE_DIR=googletest
set SCRIPTS_FOLDER_RELATIVE_LIB=..\scripts\windows

REM !!!!!!!!!!    GO TO LIBs DIR     !!!!!!!!!!!

IF EXIST ./%LIB_DIR% (
    echo ./%LIB_DIR% Exist
) ELSE (
    echo Creating ./%LIB_DIR%
    mkdir %LIB_DIR%
)

cd %LIB_DIR%

REM !!!!!!!!!!    BUILD QT     !!!!!!!!!!!

@REM IF EXIST ./%QT_DIR% (
@REM     echo ./%QT_DIR% Exist
@REM ) ELSE (
@REM     echo Downloading and creating QT
    call %SCRIPTS_FOLDER_RELATIVE_LIB%\build_qt.bat
@REM )


REM !!!!!!!!!!    SETUP GOOGLETEST     !!!!!!!!!!!

IF EXIST ./%GOOGLE_DIR% (
    echo ./%GOOGLE_DIR% Exist
) ELSE (
    echo Downloading googletest
    call %SCRIPTS_FOLDER_RELATIVE_LIB%\setup_googletest.bat
) 