@ECHO off
git clone https://github.com/qt/qt5.git
cd qt5
git checkout 5.15.2
perl ./init-repository -f --module-subset=essential,qtcharts,qtsvg,qtquickcontrols
call configure -release -optimize-size -c++std c++14 -static -static-runtime -D "CMAKE_C_COMPILER=gcc" -D "CMAKE_CXX_COMPILER=g++" -no-pch -opengl desktop -prefix %CD%\..\qt_build -skip webengine -nomake tools -nomake tests -nomake examples -confirm-license -opensource
mingw32-make.exe
mingw32-make.exe install
cd ..
