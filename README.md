SETUP WINDOWS
https://wiki.qt.io/Building_Qt_6_from_Git

cd lib
git clone https://github.com/qt/qt5.git
cd qt5
git checkout 5.15.2
#perl ./init-repository --module-subset=default,-qtwebengine,-qtqa
#./configure -developer-build -opensource -nomake examples -nomake tests
#make -j{number of cores}

git submodule update --init --recursive 
configure -prefix %CD%\qtbase -nomake tests -nomake examples
mingw32-make



https://wiki.qt.io/Building_Qt_6_from_Git
cd lib 
git clone https://github.com/qt/qt5.git



cd qt5
git checkout 6.0.0
init-repository
cd ..
mkdir build
cd build
cmake ../qt5 -G "MinGW Makefiles"  -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++ -prefix %CD%\qtbase -nomake tests -nomake examples
cmake --build . --parallel


# BUILD PROJECT
cmake .. -G "MinGW Makefiles"  -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++

## LINKS
https://exp.newsmth.net/topic/3e62dccb44a2afbbfaa96f46faf9db95
https://forum.qt.io/topic/80364/linking-static-library-to-an-executable/4
https://wiki.qt.io/Building_a_static_Qt_for_Windows_using_MinGW
https://doc-snapshots.qt.io/qtcreator-4.0/creator-project-cmake.html

# QT  with CMake
https://doc.qt.io/qt-5/cmake-get-started.html
https://doc.qt.io/archives/qt-5.10/cmake-manual.html

# QML with static QT and cmake
https://doc.qt.io/qt-5/qtqml-cmake-qt5-import-qml-plugins.html
https://doc.qt.io/QtQuickCompiler/qquickcompiler-building-with-cmake.html

https://doc.qt.io/qt-5/qtquickcontrols2-gettingstarted.html


### TO FIX
Note: Using static linking will disable the use of dynamically
loaded plugins. Make sure to import all needed static plugins,
or compile needed modules into the library.



# Git - binaries 32 - bits
https://github.com/git-for-windows/git-sdk-32

https://code.qt.io/cgit/qt/qtcharts.git/tree/examples/charts/qmlchart?h=5.15

# Styling for charts
https://doc.qt.io/archives/qt-5.11/qml-qtcharts-chartview.html#theme-prop