@echo off
mkdir build
cd build
cmake .. -G "MinGW Makefiles" -D CMAKE_CXX_COMPILER=g++ -D CMAKE_C_COMPILER=gcc
cmake --build .
ctest
cmake --install . --prefix %cd%/bin
cd ..
