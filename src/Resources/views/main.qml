
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtCharts 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0

ApplicationWindow {
    // visible: true
    // width: 640
    // height: 480
    // title: qsTr("Hello World")
    // Material.theme: Material.Dark
    // Material.accent: Material.Blue
    
    // Column {
    //     anchors.centerIn: parent

    //     RadioButton { text: qsTr("Small") }
    //     RadioButton { text: qsTr("Medium");  checked: true }
    //     RadioButton { text: qsTr("Large") }
    // }

    // Gauge {
    //     id: gauge
    //     anchors.fill: parent
    //     anchors.margins: 10

    //     property bool accelerating: false

    //     value: accelerating ? maximumValue : 0
    //     Keys.onSpacePressed: accelerating = true
    //     Keys.onReleased: {
    //         if (event.key === Qt.Key_Space) {
    //             accelerating = false;
    //             event.accepted = true;
    //         }
    //     }

    //     Component.onCompleted: forceActiveFocus()

    //     Behavior on value {
    //         NumberAnimation {
    //             duration: 1000
    //         }
    //     }
        
    //     style: GaugeStyle {
    //         valueBar: Rectangle {
    //             implicitWidth: 44
    //             color: Qt.rgba(gauge.value / gauge.maximumValue, 1 - gauge.value / gauge.maximumValue, 0, 1)
    //         }
    //         minorTickmark: Item {
    //             implicitWidth: 0
    //             implicitHeight: 0
    //         }
    //         tickmark: Item {
    //             implicitWidth: 0
    //             implicitHeight: 0
    //         }
    //         tickmarkLabel : Item {

    //         }
    //     }
        
    // }
    // CircularGauge {
    //     value: accelerating ? maximumValue : 0
    //     anchors.centerIn: parent
    //     tickmarksVisible : false

    //     property bool accelerating: false

    //     Keys.onSpacePressed: accelerating = true
    //     Keys.onReleased: {
    //         if (event.key === Qt.Key_Space) {
    //             accelerating = false;
    //             event.accepted = true;
    //         }
    //     }

    //     Component.onCompleted: forceActiveFocus()

    //     Behavior on value {
    //         NumberAnimation {
    //             duration: 1000
    //         }
    //     }

        
    // }

    // ChartView {
    //     title: "Bar series"
    //     anchors.fill: parent
    //     legend.alignment: Qt.AlignBottom
    //     antialiasing: true
    //     theme: ChartView.ChartThemeDark

    //     BarSeries {
    //         id: mySeries
    //         axisX: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
    //         BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
    //         BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
    //         BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
    //     }
    // }
    id:root
    visible: true
    width: 1024
    height: 600
    property int external_width: 534
    property int external_height: 533
    property bool external_reverse: false
    property int external_angle: 0
    property int externalstart_angle: 0 //138
    property int external_angle_limit: 180//264
    property int external_radius: 235
    property int external_lineWidth: 60

    Canvas {
        id: external_progress_bar
        width: root.external_width
        height: root.external_height
        x: (root.width - width)/2
        y: (root.height - height)/2
        property real startAngle: (Math.PI/180) *180
        property real nextAngle: (Math.PI/180) *180
        property color col: "red"
        // textAlign : center
        onPaint: {
            var ctx = getContext("2d");
            ctx.reset();
            ctx.beginPath();
            ctx.arc(width/2, height/2, root.external_radius, startAngle, nextAngle + (Math.PI/180) * 180, false);
            ctx.lineWidth = root.external_lineWidth
            ctx.strokeStyle = col
            ctx.fillText("50 %", x, y)
            ctx.stroke()
        }
    }

    Canvas {
        id: external_progress_bar_2
        width: root.external_width
        height: root.external_height
        x: (root.width - width)/2
        y: (root.height - height)/2
        property real startAngle: (Math.PI/180) *180
        property real nextAngle: (Math.PI/180) *90
        property color col: "blue"
        onPaint: {
            var ctx = getContext("2d");
            ctx.reset();
            ctx.beginPath();
            ctx.arc(width/2, height/2, root.external_radius, startAngle, nextAngle + (Math.PI/180) * 180, false);
            ctx.lineWidth = root.external_lineWidth
            ctx.strokeStyle = col
            ctx.stroke()
        }
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }
}